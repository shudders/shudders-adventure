﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Swipe : MonoBehaviour {

    private Vector3 ti;
    private Vector3 tf;
    private float minDist;
    public Text text;
    
	// Use this for initialization
	void Start () {
        minDist = Screen.height * 15 / 100;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.touchCount == 1) {
            Touch toque = Input.GetTouch(0);
            if (toque.phase == TouchPhase.Began) {
                ti = toque.position;
                tf = toque.position;
            }
            else if (toque.phase == TouchPhase.Moved) {
                tf = toque.position;
            }
            else if (toque.phase == TouchPhase.Ended) {
                tf = toque.position;

                if (Mathf.Abs(tf.x - ti.x) > minDist || Mathf.Abs(tf.y - ti.y) > minDist)
                {
                    if (Mathf.Abs(tf.x - ti.x) > Mathf.Abs(tf.y - ti.y))
                    {
                        if (tf.x > ti.x)
                        {
                            string mensagem = "Swipe para direita";
                            text.text = mensagem;
                        }
                        else
                        {
                            string mensagem = "Swipe para esquerda";
                            text.text = mensagem;
                        }
                    }
                    else
                    {
                        if (tf.y > ti.y)
                        {
                            string mensagem = "Swipe para cima";
                            text.text = mensagem;
                        }
                        else
                        {
                            string mensagem = "Swipe para baixo";
                            text.text = mensagem;
                        }
                    }
                }
                else {
                    string mensagem = "Toque simples";
                    text.text = mensagem;
                }
            }
        }

	}
}
