﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class meuCaminho : MonoBehaviour {


    public Text debug;
    public int xMax;
    public int xMin;
    public int yMax;
    public int yMin;

    void Update () {
        if (Input.touchCount == 1) {
            Vector3 position = transform.position;
            float transX = Input.touches[0].deltaPosition.x * .05F;
            float transY = Input.touches[0].deltaPosition.y * .05F;
            if (transform.position.x + transX > xMax) {
                position.x = 2.5f;
            }
            else
            {
                if(transform.position.x + transX < -xMin)
                {
                    position.x = xMin;
                }
                else
                {
                    position.x += transX;
                }
            }
            if (transform.position.y + transY > yMax)
            {
                position.x = 2.5f;
            }
            else
            {
                if (transform.position.y + transX < -yMin)
                {
                    position.y = yMin;
                }
                else
                {
                    position.y += transY;
                }
            }
            position.z = 0;

            transform.position = position;

            string mensagem = "";
            mensagem += "x: " + transform.position.x + "\n";
            mensagem += "y: " + transform.position.y + "\n";
            mensagem += "z: " + transform.position.z + "\n";
            mensagem += "delta x: " + Input.touches[0].deltaPosition.x * .25f + "\n";
            mensagem += "delta y: " + Input.touches[0].deltaPosition.y * .25f + "\n";
            debug.text = mensagem;
        }
		
	}
}
