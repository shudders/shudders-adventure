﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class movimento : MonoBehaviour {

	public Text debug;
	void Update () {
		if(Input.touchCount == 1){
			transform.Translate (Input.touches [0].deltaPosition.x * .25f,
				Input.touches [0].deltaPosition.y * .25f,
				0);

			string mensagem = "";
			mensagem += "x: " + transform.position.x + "\n";
			mensagem += "y: " + transform.position.y + "\n";
			mensagem += "z: " + transform.position.z + "\n";
			mensagem += "delta x: "+ Input.touches[0].deltaPosition.x * .25f+"\n";
			mensagem += "delta y: "+ Input.touches[0].deltaPosition.y * .25f+"\n";
            debug.text = mensagem;
		}
		
	}
}
